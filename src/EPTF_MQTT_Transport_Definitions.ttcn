///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               EPTF_MQTT_Transport_Definitions.ttcn
//  Description:
//  Rev:                R1C
//  Prodnr:             CNL 113 860
//  Updated:            2023-03-13
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_MQTT_Transport_Definitions
// 
//  Purpose:
//    This module contains the definitions of callback functions for the Applib user API
//
//  Module depends on:
//    - <MQTT_v3_1_1_Types>
//    - <IPL4asp_Types>
//
//  Component Diagram:
//    (see EPTF_MQTT_Transport_Definitions.components.jpg)
//
//  See also:
//    - <EPTF_MQTT_LGen_Definitions>
//    - <EPTF_MQTT_Transport_User_CT>
//    - <EPTF_MQTT_Transport_Provider_CT>
// 
//  Type:
//    EPTF_MQTT_PDU - <EPTF_MQTT_PDU> - Encapsulates a MQTT PDU with the corresponding entity (and FSM) indices and transport information
//
//  Callback Function types:
//    fcb_EPTF_MQTT_Transport_receiveMessage - <fcb_EPTF_MQTT_Transport_receiveMessage> - Call-back function type for reporting received <EPTF_MQTT_PDU> MQTT message
//    fcb_EPTF_MQTT_Transport_receiveEvent - <fcb_EPTF_MQTT_Transport_receiveEvent> - Call-back function type for reporting received <ASP_Event> MQTT event
//    fcb_EPTF_MQTT_Transport_sendMessage - <fcb_EPTF_MQTT_Transport_sendMessage> - Call-back function type for sending a <EPTF_MQTT_PDU> MQTT message
//    fcb_EPTF_MQTT_Transport_apiRequest - <fcb_EPTF_MQTT_Transport_apiRequest> - Call-back function type for sending a <EPTF_MQTT_Transport_Request> MQTT transport request
//    fcb_EPTF_MQTT_Transport_apiResponse - <fcb_EPTF_MQTT_Transport_apiResponse> - Call-back function type for reporting a <EPTF_MQTT_Transport_Response> MQTT transport response
//
///////////////////////////////////////////////////////////////
/*****************************************************************
 @startuml EPTF_MQTT_Transport_Definitions.components.jpg
   package "EPTF_MQTT_Transport_Definitions" {
     class EPTF_MQTT_Transport_User_CT {
       fcb_EPTF_MQTT_Transport_sendMessage vf_EPTF_MQTT_Transport_send
       fcb_EPTF_MQTT_Transport_apiRequest vf_EPTF_MQTT_Transport_apiRequest
     }
     class EPTF_MQTT_Transport_Provider_CT {
       fcb_EPTF_MQTT_Transport_receiveMessage vf_EPTF_MQTT_Transport_receiveMessage
       fcb_EPTF_MQTT_Transport_receiveEvent fcb_EPTF_MQTT_Transport_receiveEvent
       fcb_EPTF_MQTT_Transport_apiResponse vf_EPTF_MQTT_Transport_apiResponse
     }
     EPTF_MQTT_Transport_Provider_CT <-> EPTF_MQTT_Transport_User_CT
  }
 @enduml
******************************************************************/

module EPTF_MQTT_Transport_Definitions
{
  import from MQTT_v3_1_1_Types all;
  import from IPL4asp_Types all;

  ///////////////////////////////////////////////////////////
  //  Type: fcb_EPTF_MQTT_Transport_receiveMessage
  //
  //  Purpose:
  //    Call-back function type for reporting received <EPTF_MQTT_PDU> MQTT message
  //
  //  Parameters:
  //    pl_message  - *in* <EPTF_MQTT_PDU> - received MQTT message
  ///////////////////////////////////////////////////////////
  type function fcb_EPTF_MQTT_Transport_receiveMessage(in EPTF_MQTT_PDU pl_message) runs on self;

  ///////////////////////////////////////////////////////////
  //  Type: fcb_EPTF_MQTT_Transport_receiveEvent
  //
  //  Purpose:
  //    Call-back function type for reporting received <ASP_Event> MQTT event
  //
  //  Parameters:
  //    p_event  - *in* <ASP_Event> - received MQTT event
  ///////////////////////////////////////////////////////////
  type function fcb_EPTF_MQTT_Transport_receiveEvent(in ASP_Event p_event) runs on self;
  
  ///////////////////////////////////////////////////////////
  //  Type: fcb_EPTF_MQTT_Transport_sendMessage
  //
  //  Purpose:
  //    Call-back function type for sending a <EPTF_MQTT_PDU> MQTT message
  //
  //  Parameters:
  //    pl_msg  - *in* <EPTF_MQTT_PDU> - MQTT message to be sent
  ///////////////////////////////////////////////////////////
  type function fcb_EPTF_MQTT_Transport_sendMessage(in EPTF_MQTT_PDU pl_msg) runs on self;
  
  ///////////////////////////////////////////////////////////
  //  Type: fcb_EPTF_MQTT_Transport_apiRequest
  //
  //  Purpose:
  //    Call-back function type for sending a <EPTF_MQTT_Transport_Request> MQTT transport request
  //
  //  Parameters:
  //    pl_req  - *in* <EPTF_MQTT_Transport_Request> - MQTT transport request to be sent
  ///////////////////////////////////////////////////////////
  type function fcb_EPTF_MQTT_Transport_apiRequest(in EPTF_MQTT_Transport_Request pl_req) runs on self;
  
  ///////////////////////////////////////////////////////////
  //  Type: fcb_EPTF_MQTT_Transport_apiResponse
  //
  //  Purpose:
  //    Call-back function type for reporting a <EPTF_MQTT_Transport_Response> MQTT transport response
  //
  //  Parameters:
  //    pl_rsp  - *in* <EPTF_MQTT_Transport_Response> - MQTT transport response to be reported
  ///////////////////////////////////////////////////////////
  type function fcb_EPTF_MQTT_Transport_apiResponse(in EPTF_MQTT_Transport_Response pl_rsp) runs on self;

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_MQTT_PDU
  //
  //  Purpose:
  //    Encapsulates a MQTT PDU with the corresponding entity (and FSM) indices and transport information
  //
  //  Parameters:
  //    pdu - <MQTT_ReqResp> - MQTT PDU
  //    transportParams - <EPTF_MQTT_TransportParameters> - transport protocol information
  //    sessionIdx - *integer* - session index
  ///////////////////////////////////////////////////////////
  type record EPTF_MQTT_PDU
  {
    MQTT_v3_1_1_ReqResp           pdu,
    EPTF_MQTT_TransportParameters transportParams,
    integer                       sessionIdx
  }

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_MQTT_TransportParameters
  //
  //  Purpose:
  //    Transport parameters for MQTT
  //
  //  Parameters:
  //    localAddress - <Socket> - local address
  //    remoteAddress - <Socket> - remote address
  //    proto - <ProtoTuple> - IP transport protocol
  ///////////////////////////////////////////////////////////
  type record EPTF_MQTT_TransportParameters
  {
    Socket      localAddress,
    Socket      remoteAddress,
    ProtoTuple  proto
  }

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_MQTT_Transport_Request
  //
  //  Purpose:
  //    Type for transport request
  //
  //  Elements:
  //    sessionIdx - *integer* - session index
  //    expectResponse - *boolean* - set to true if request execution result is expected to be reported back
  //    params - <EPTF_MQTT_Transport_RequestParams> *optional* - parameters of the request
  ///////////////////////////////////////////////////////////
  type record EPTF_MQTT_Transport_Request
  {
    integer sessionIdx,
    boolean expectResponse,
    EPTF_MQTT_Transport_RequestParams params optional
  }

  const EPTF_MQTT_Transport_Request c_EPTF_MQTT_Transport_Request_init :=
  {
    sessionIdx := -1,
    expectResponse := true,
    params := omit
  }

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_MQTT_Transport_RequestParams
  //
  //  Purpose:
  //    Union encapsulating the different transport requests
  //
  //  Elements:
  //    startListening - <EPTF_MQTT_Transport_startListening> - starts a listening socket
  //    connect_ - <EPTF_MQTT_Transport_connect> - connects to the remote address
  //    close - <EPTF_MQTT_Transport_RequestParams> - closes the connection
  ///////////////////////////////////////////////////////////
  type union EPTF_MQTT_Transport_RequestParams
  {
    EPTF_MQTT_Transport_startListening startListening,
    EPTF_MQTT_Transport_connect        connect_,
    EPTF_MQTT_Transport_close          close
  }

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_MQTT_Transport_startListening
  //
  //  Purpose:
  //    Starts a listening socket
  //
  //  Elements:
  //    localAddress - <Socket> - local address of the listning socket
  ///////////////////////////////////////////////////////////
  type record EPTF_MQTT_Transport_startListening
  {
    Socket localAddress
  }

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_MQTT_Transport_connect
  //
  //  Purpose:
  //    Establishes a connection between the local and remote sockets
  //
  //  Elements:
  //    localAddress - <Socket> - local address of the connection
  //    remoteAddress - <Socket> - remote address of the connection
  ///////////////////////////////////////////////////////////
  type record EPTF_MQTT_Transport_connect
  {
    Socket localAddress,
    Socket remoteAddress
  }

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_MQTT_Transport_close
  //
  //  Purpose:
  //    Closes the connection
  //
  //  Elements:
  //    localAddress - <Socket> - local address of the connection
  ///////////////////////////////////////////////////////////
  type record EPTF_MQTT_Transport_close
  {
    Socket localAddress
  }

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_MQTT_Transport_Response
  //
  //  Purpose:
  //    Type for transport response
  //
  //  Parameters:
  //    succ - *boolean* - result request's execution
  //    sessionIdx - *integer* - session index
  //    params - <EPTF_MQTT_Transport_ResponseParams> *optional* - parameters of the response
  ///////////////////////////////////////////////////////////
  type record EPTF_MQTT_Transport_Response
  {
    boolean succ,
    integer sessionIdx,
    EPTF_MQTT_Transport_ResponseParams params optional
  }

  const EPTF_MQTT_Transport_Response c_EPTF_MQTT_Transport_Response_init :=
  {
    succ := false,
    sessionIdx := -1,
    params := omit
  }

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_MQTT_Transport_ResponseParams
  //
  //  Purpose:
  //    Union encapsulating the responses
  //
  //  Elements:
  //    listening - *boolean* - true if the listening socket was opened succesfully, false otherwise
  //    connectionClose - <Socket> - indication of a closed connection with its address
  ///////////////////////////////////////////////////////////
  type union EPTF_MQTT_Transport_ResponseParams
  {
    boolean listening,
    boolean tcpEstablished,
    Socket  connectionClosed
  }

  /*****************************************************************
   @startuml EPTF_MQTT_Transport_Definitions.EPTF_MQTT_Transport_Provider_CT.jpg
     class EPTF_MQTT_Transport_Provider_CT {
       fcb_EPTF_MQTT_Transport_receiveMessage vf_EPTF_MQTT_Transport_receiveMessage
       fcb_EPTF_MQTT_Transport_receiveEvent fcb_EPTF_MQTT_Transport_receiveEvent
       fcb_EPTF_MQTT_Transport_apiResponse vf_EPTF_MQTT_Transport_apiResponse
     }
   @enduml
  *****************************************************************/
  ///////////////////////////////////////////////////////////
  // Type: EPTF_MQTT_Transport_Provider_CT
  //
  // Purpose:
  //   Base component type for a MQTT transport realization, declares variables to register the transport user functions
  //
  // Class:
  //   (see EPTF_MQTT_Transport_Definitions.EPTF_MQTT_Transport_Provider_CT.jpg)
  //
  // Variables:
  //   vf_EPTF_MQTT_Transport_receiveMessage - <fcb_EPTF_MQTT_Transport_receiveMessage> - Function hook for reporting received messages
  //   vf_EPTF_MQTT_Transport_receiveEvent - <fcb_EPTF_MQTT_Transport_receiveEvent> - Function hook for reporting received events
  //   vf_EPTF_MQTT_Transport_apiResponse - <fcb_EPTF_MQTT_Transport_apiResponse> - Function hook for reporting transport API responses
  ///////////////////////////////////////////////////////////
  type component EPTF_MQTT_Transport_Provider_CT
  {
    var fcb_EPTF_MQTT_Transport_receiveMessage  vf_EPTF_MQTT_Transport_receiveMessage := null;
    var fcb_EPTF_MQTT_Transport_receiveEvent    vf_EPTF_MQTT_Transport_receiveEvent := null;
    var fcb_EPTF_MQTT_Transport_apiResponse     vf_EPTF_MQTT_Transport_apiResponse := null;
  }

  /*****************************************************************
   @startuml EPTF_MQTT_Transport_Definitions.EPTF_MQTT_Transport_User_CT.jpg
     class EPTF_MQTT_Transport_User_CT {
       fcb_EPTF_MQTT_Transport_sendMessage vf_EPTF_MQTT_Transport_send
       fcb_EPTF_MQTT_Transport_apiRequest vf_EPTF_MQTT_Transport_apiRequest
     }
   @enduml
  *****************************************************************/
  ///////////////////////////////////////////////////////////
  // Type: EPTF_MQTT_Transport_User_CT
  //
  // Purpose:
  //   Base component type for a MQTT transport user realization
  //
  // Class:
  //   (see EPTF_MQTT_Transport_Definitions.EPTF_MQTT_Transport_User_CT.jpg)
  //
  // Variables:
  //   vf_EPTF_MQTT_Transport_send - <fcb_EPTF_MQTT_Transport_sendMessage> - Function hook for reporting received messages
  //   vf_EPTF_MQTT_Transport_apiRequest - <fcb_EPTF_MQTT_Transport_apiRequest> - Function hook for reporting received events
  ///////////////////////////////////////////////////////////
  type component EPTF_MQTT_Transport_User_CT
  {
    var fcb_EPTF_MQTT_Transport_sendMessage     vf_EPTF_MQTT_Transport_send := null;
    var fcb_EPTF_MQTT_Transport_apiRequest      vf_EPTF_MQTT_Transport_apiRequest := null;
  }
}
